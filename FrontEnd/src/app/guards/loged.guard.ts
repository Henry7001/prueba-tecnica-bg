import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })
export class LogedGuard implements CanActivate {
    constructor(private router: Router) {}
    canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const isLoged = localStorage.getItem('logged');
      if (isLoged) {
        //this.router.navigate(['/productos']);
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    }
}
    