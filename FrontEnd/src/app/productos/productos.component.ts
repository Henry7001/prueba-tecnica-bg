import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  constructor(
    private service: ProductoService
  ) { }

  productos:any[] = [];

  getAllProductos(){
    return this.service.getAllProducts().subscribe({
      next: (value:any)=>{
        this.productos = value.data;
        console.log(this.productos)
      }
    })
  }

  ngOnInit(): void {
    this.getAllProductos();
  }
}
