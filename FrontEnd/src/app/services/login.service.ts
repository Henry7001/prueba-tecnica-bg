import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BodyDto } from '../models/BodyDto.model';
import { ResponseDto } from '../models/Response.dto';
import { environment } from '../../environments/environment.prod';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  login(login: BodyDto) {
    return this.http.post<ResponseDto>(`${apiUrl}/Login`, login);
  }
}
