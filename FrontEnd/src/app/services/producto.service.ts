import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
const apiUrl = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private httpClient: HttpClient) { }

  getAllProducts(){
    const body = {
      "transaccion": "generico",
      "tipo": "4"
    };
    return this.httpClient.post<any[]>(`${apiUrl}/Producto/productos`, body)
  }
}