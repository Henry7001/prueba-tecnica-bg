import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BodyDto } from 'src/app/models/BodyDto.model';
import { ResponseDto } from 'src/app/models/Response.dto';
import { Usuario } from 'src/app/models/Usuario.model';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  usuario?: Usuario;

  constructor(
    private formBuilder: FormBuilder,
    private _loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    if (this.form.valid) {
      let loginData: BodyDto = {
        transaccion: 'autenticarUsuario',
        datosUsuario: {
          email: this.form.value.email,
          password: this.form.value.password,
        },
      };
      this._loginService.login(loginData).subscribe({
        next: (resp: ResponseDto) => {
          if (resp.codigoRetorno == '0001') {
            this.router.navigate(['/productos']);
            this.usuario = resp.usuario;
            localStorage.setItem('logged', 'true');
          } else {
          
          }
        },
        error: (err) => {
        },
      });
    } else {
    }
  }
}
