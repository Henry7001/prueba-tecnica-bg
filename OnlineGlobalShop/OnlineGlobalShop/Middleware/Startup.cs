﻿using Microsoft.AspNetCore.Builder;

namespace OnlineGlobalShop.Middleware
{
    public static class Startup
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder app)
            => app.UseMiddleware<ExceptionMiddleware>();
    }
}
