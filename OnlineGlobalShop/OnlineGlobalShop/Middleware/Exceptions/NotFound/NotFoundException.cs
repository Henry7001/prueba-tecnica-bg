﻿using System;

namespace OnlineGlobalShop.Middleware.Exceptions.NotFound
{
    public class NotFoundException : Exception
    {

        public NotFoundException(string mensaje) : base(mensaje)
        {
        }
    }
}
