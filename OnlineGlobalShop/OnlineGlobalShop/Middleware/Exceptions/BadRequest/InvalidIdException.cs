using OnlineGlobalShop.Middleware.Exceptions.BadRequest;

namespace OnlineGlobalShop.Exceptions.BadRequest
{
    public class InvalidIdException : BadRequestException
    {
        public InvalidIdException() : base("Id no válido.")
        {
        }
    }
}