﻿using OnlineGlobalShop.Datos;
using OnlineGlobalShop.Entidades;
using OnlineGlobalShop.Middleware.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineGlobalShop.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {

        private readonly MapeoDatosUsuario _usuarioDatos;

        public UsuarioController()
        {
            _usuarioDatos = new MapeoDatosUsuario();
        }

        [HttpPost("/Login")]
        [ProducesResponseType(typeof(ErrorResponse), 500)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(typeof(Usuario), 200)]
        public IActionResult Login([FromBody] LoginRequest request)
        {
            bool credencialesValidas = _usuarioDatos.VerificarCredenciales(request.datosUsuario.email, request.datosUsuario.password);

            if (credencialesValidas)
            {
                Usuario usuario = _usuarioDatos.ObtenerUsuarioPorEmail(request.datosUsuario.email);
                LoginResponse response = new()
                {
                    codigoRetorno = "0001",
                    mensajeRetorno = "consulta correcta"
                };

                Usuario userRepsonse = new()
                {
                    Plan = usuario.Plan,
                    Email = usuario.Email,
                    Nombres = usuario.Nombres,
                    Phone = usuario.Phone
                };

                response.usuario = userRepsonse;
                return Ok(response);
            }

            return Unauthorized(new LoginResponse());
        }
    }
}
