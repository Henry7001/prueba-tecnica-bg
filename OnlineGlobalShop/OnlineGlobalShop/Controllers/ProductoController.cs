﻿using Microsoft.AspNetCore.Mvc;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using OnlineGlobalShop.Datos;
using OnlineGlobalShop.Entidades;

namespace OnlineGlobalShop.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductoController : Controller
    {

        private readonly MapeoDatosProducto _productoDatos;

        public ProductoController()
        {
            _productoDatos = new MapeoDatosProducto();
        }

        string Cadena = Environment.GetEnvironmentVariable("DATABASE");

        [HttpPost("productos")]
        public IActionResult ObtenerTodosLosProductos([FromBody] Request request)
        {
            if (request != null && request.transaccion == "generico" && request.tipo == "4")
            {
                Response productos = ObtenerTodosLosProductos();

                if (productos != null)
                {
                    return Ok(productos);
                }
            }

            return BadRequest("Solicitud inválida");
        }

        private Response ObtenerTodosLosProductos()
        {
            string query = "pa_product";

            using (DbConnection dbConnection = new SqlConnection(Cadena))
            {
                dbConnection.Open();
                using (DbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@action", 2));
                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        List<DatosItem> productos = new();

                        while (dr.Read())
                        {
                            DatosItem producto = new()
                            {
                                id = dr.GetInt32(dr.GetOrdinal("id")),
                                descripcion = dr.GetString(dr.GetOrdinal("descripcion")),
                                precio = dr.GetDecimal(dr.GetOrdinal("precio")),
                                estado = dr.GetBoolean(dr.GetOrdinal("estado")),
                                detalle = dr.GetString(dr.GetOrdinal("detalle")),
                                imagen = dr.GetString(dr.GetOrdinal("imagen"))
                            };
                            productos.Add(producto);
                        }

                        Response response = new()
                        {
                            codigoRetorno = "0001",
                            mensajeRetorno = "Ok",
                            data = productos
                        };
                        return response;
                    }
                }
            }
        }

    }
}
