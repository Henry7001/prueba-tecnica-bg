﻿namespace OnlineGlobalShop.Entidades
{
    public class Producto
    {
        public int Id { get; set; }
        public string Desc { get; set; }
        public decimal Price { get; set; }
        public bool Status { get; set; }
        public string Detail { get; set; }
        public string Image { get; set; }
    }
}
