﻿namespace OnlineGlobalShop.Entidades
{
    public class DatosItem
    {
        public int id { get; set; }
        public string descripcion { get; set; }
        public decimal precio { get; set; }
        public bool estado { get; set; }
        public string detalle { get; set; }
        public string imagen { get; set; }
    }
}
