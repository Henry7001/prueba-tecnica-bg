﻿namespace OnlineGlobalShop.Entidades
{
    using System;

    public class LoginResponse
    {
        public string codigoRetorno { get; set; }
        public string mensajeRetorno { get; set; }
        public Usuario usuario { get; set; }
    }
}
