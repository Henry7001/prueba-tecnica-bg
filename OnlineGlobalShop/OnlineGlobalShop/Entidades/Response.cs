﻿namespace OnlineGlobalShop.Entidades
{
    using System;
    using System.Collections.Generic;

    public class Response
    {
        public string codigoRetorno { get; set; }
        public string mensajeRetorno { get; set; }
        public List<DatosItem> data { get; set; }
    }
}
