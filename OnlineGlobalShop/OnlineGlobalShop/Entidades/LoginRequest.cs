﻿namespace OnlineGlobalShop.Entidades
{
    using System;

    public class LoginRequest
    {
        public string transaccion { get; set; }
        public SendData datosUsuario { get; set; }
    }

    public class SendData
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
