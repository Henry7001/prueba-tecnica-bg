﻿using OnlineGlobalShop.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace OnlineGlobalShop.Datos
{
    public class MapeoDatosUsuario
    {
        string Cadena = Environment.GetEnvironmentVariable("DATABASE");
        public bool VerificarCredenciales(string email, string password)
        {
            string query = "pa_login";

            using (DbConnection dbConnection = new SqlConnection(Cadena))
            {
                dbConnection.Open();
                using (DbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@action", 2));
                    cmd.Parameters.Add(new SqlParameter("@email", email));
                    cmd.Parameters.Add(new SqlParameter("@password", password));

                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            return dr.GetString(dr.GetOrdinal("mensaje")) == "Credenciales validas";
                        }
                    }
                }
            }
            return false;
        }



        public Usuario ObtenerUsuarioPorEmail(string email)
        {
            string query = "pa_login";

            using (DbConnection dbConnection = new SqlConnection(Cadena))
            {
                dbConnection.Open();
                using (DbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@action", 1));
                    cmd.Parameters.Add(new SqlParameter("@email", email));

                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            Usuario usuario = new Usuario();
                            usuario.Id = dr.GetInt32(dr.GetOrdinal("id"));
                            usuario.Email = dr.GetString(dr.GetOrdinal("email"));
                            usuario.Password = dr.GetString(dr.GetOrdinal("password"));
                            usuario.Nombres = dr.GetString(dr.GetOrdinal("nombre"));
                            usuario.Plan = dr.IsDBNull(dr.GetOrdinal("plan")) ? (int?)null : dr.GetInt32(dr.GetOrdinal("plan"));
                            usuario.Phone = dr.IsDBNull(dr.GetOrdinal("telefono")) ? null : dr.GetString(dr.GetOrdinal("telefono"));
                            return usuario;
                        }
                    }
                }
            }
            return null;
        }



    }
}
