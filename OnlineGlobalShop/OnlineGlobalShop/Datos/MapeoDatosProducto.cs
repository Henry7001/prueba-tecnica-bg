﻿using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using OnlineGlobalShop.Entidades;

namespace OnlineGlobalShop.Datos
{
    public class MapeoDatosProducto
    {
        string Cadena = Environment.GetEnvironmentVariable("DATABASE");
        public List<Producto> GetAllProducts()
        {
            string query = "pa_product";

            using (DbConnection dbConnection = new SqlConnection(Cadena))
            {
                dbConnection.Open();
                using (DbCommand cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@action", 2));

                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        List<Producto> productos = new();

                        while (dr.Read())
                        {
                            Producto producto = new Producto();
                            producto.Id = dr.GetInt32(dr.GetOrdinal("product_id"));
                            producto.Desc = dr.GetString(dr.GetOrdinal("descripcion"));
                            producto.Price = dr.GetDecimal(dr.GetOrdinal("price"));
                            producto.Status = dr.GetBoolean(dr.GetOrdinal("status"));
                            producto.Detail = dr.GetString(dr.GetOrdinal("detail"));
                            producto.Image = dr.GetString(dr.GetOrdinal("image"));
                            productos.Add(producto);
                        }

                        return productos;
                    }
                }
            }
        }

    }
}
