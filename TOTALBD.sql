CREATE DATABASE Prueba;
GO

USE Prueba;
GO

CREATE TABLE Usuario (
    id INT IDENTITY(1,1) PRIMARY KEY,
    email VARCHAR(255) NOT NULL unique,
    [password] VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NULL,
    [plan] INT NULL,
    telefono VARCHAR(20) NULL
);

INSERT INTO Usuario (email, [password], nombre, [plan], telefono)
VALUES ('hruiz@gmail.com', 'contrasena', 'Henry', 1, '1234567890');

INSERT INTO Usuario (email, [password], nombre, [plan], telefono)
VALUES ('NelsonCanMaLam@bancoguayaquil.com', 'password', 'Nelson', 2, '9876543210');


GO

CREATE TABLE Producto (
    id INT IDENTITY(1,1) PRIMARY KEY,
    descripcion VARCHAR(255) NOT NULL,
    precio DECIMAL(10, 2) NOT NULL,
    estado BIT NOT NULL,
    detalle TEXT,
    imagen VARCHAR(255)
);
GO

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Camisa a rayas', 29.99, 1, 'Camisa a rayas de algod�n para hombre', 'https://picsum.photos/640/640?r=8481');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Pantalones vaqueros', 49.95, 1, 'Pantalones vaqueros ajustados para mujer', 'https://picsum.photos/640/640?r=4549');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Zapatillas deportivas', 39.99, 0, 'Zapatillas deportivas ligeras para correr', 'https://picsum.photos/640/640?r=8481');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Chaqueta de cuero', 89.99, 1, 'Chaqueta de cuero genuino para hombre', 'https://picsum.photos/640/640?r=4549');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Bufanda de lana', 14.75, 1, 'Bufanda suave y abrigada de lana', 'https://picsum.photos/640/640?r=8481');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Sombrero de paja', 24.50, 0, 'Sombrero de paja tejida a mano para mujer', 'https://picsum.photos/640/640?r=4549');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Cintur�n de piel', 32.99, 1, 'Cintur�n de piel de vaca genuina', 'https://picsum.photos/640/640?r=4549');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Gafas de sol', 19.25, 1, 'Gafas de sol polarizadas con montura de metal', 'https://picsum.photos/640/640?r=8481g');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Bolso de mano', 59.95, 1, 'Bolso de mano elegante para mujer', 'https://picsum.photos/640/640?r=4549');

INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
VALUES ('Reloj de pulsera', 39.99, 0, 'Reloj de pulsera anal�gico con correa de cuero', 'https://picsum.photos/640/640?r=8481');


CREATE PROCEDURE pa_login
    @action INT,
    @user_id INT = NULL,
    @email VARCHAR(255) = NULL,
    @password VARCHAR(255) = NULL
AS
BEGIN
    IF @action = 1
    BEGIN
        IF @email IS NOT NULL
            SELECT * FROM Usuario WHERE email = @email;
        ELSE
            SELECT * FROM Usuario;
    END
    ELSE IF @action = 2
    BEGIN
        IF EXISTS (SELECT 1 FROM Usuario WHERE email = @email AND [password] = @password)
            SELECT 'Credenciales validas' AS mensaje;
        ELSE
            SELECT 'Credenciales invalidas' AS mensaje;
    END
END;
GO

CREATE PROCEDURE pa_product
    @action INT,
    @product_id INT = NULL,
    @descripcion VARCHAR(255) = NULL,
    @price DECIMAL(10, 2) = NULL,
    @status BIT = NULL,
    @detail TEXT = NULL,
    @image VARCHAR(255) = NULL
AS
BEGIN
    IF @action = 1
    BEGIN
        INSERT INTO Producto (descripcion, precio, estado, detalle, imagen)
        VALUES (@descripcion, @price, @status, @detail, @image);
    END
    ELSE IF @action = 2
    BEGIN
        IF @product_id IS NOT NULL
            SELECT * FROM Producto WHERE id = @product_id;
        ELSE
            SELECT * FROM Producto;
    END
    ELSE IF @action = 3
    BEGIN
        UPDATE Producto
        SET descripcion = @descripcion, precio = @price, estado = @status, detalle = @detail, imagen = @image
        WHERE id = @product_id;
    END
    ELSE IF @action = 4
    BEGIN
        DELETE FROM Producto WHERE id = @product_id;
    END
END;
GO
